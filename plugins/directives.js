import Vue from 'vue'
import Ripple from 'vue-ripple-directive'

Ripple.color = '#BEC5FF'
Ripple.zIndex = 1
Vue.directive('ripple', Ripple)

import * as vClickOutside from 'v-click-outside-x'
Vue.use(vClickOutside)