import * as types from '@/store/mutation-types'

export const state = () => ({

  // Step 1
  address: '',
  unit_use_identity: null,
  homesize: null,

  garage: false,
  balcony: false,
  bathroom: false,
  kitchen: false,

  // Step 2
  result: null,

  form: null

})

export const actions = {

  async getValueProposition({ state, commit }) {

    const { Status, Result, Prediction_id } = await this.$axios.$put('/Get_Value_Proposition', {
      unit_use_identity: state.unit_use_identity,
      homesize: state.homesize,
      garage: state.garage,
      balcony: state.balcony,
      bathroom: state.bathroom,
      kitchen: state.kitchen
    })

    if (Status) {
      await commit(`${types.SAVE_SEARCH_KEY}`, {
        key: 'result',
        value: {
          ...Result,
          prediction_id: Prediction_id
        }
      })

      console.log(state.result);

      this.$router.push({ name: 'sell-info' })
    }

  },

  async payCheck({ state }, token) {

    const { Status } = await this.$axios.$put('/Get_Product', {
      stripe_card_token: token,
      prediction_id: state.result.prediction_id,
      send_email_to: state.form.email
    })

    if (Status)
      this.$router.push({ name: 'sell-thanks' })

  },

}

export const mutations = {
  [types.SAVE_SEARCH_KEY](state, { key, value }) {
    state[key] = value
  }
}