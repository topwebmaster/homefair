import webpack from 'webpack'

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'Home Fair',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=PT+Serif:400,700|Roboto:400,500,700&display=swap' }
    ],
    script: [
      { src: 'https://js.stripe.com/v3/', body: true }
    ]
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: '#283fff' },
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ],
  /*
   ** Nuxt.js plugins
   */
  plugins: [
    { src: '~/plugins/axios' },
    { src: '~/plugins/vuelidate' },
    { src: '~/plugins/directives', ssr: false },
    { src: '~/plugins/fontawesome', ssr: false },
    { src: '~/plugins/lodash', ssr: false },
  ],
  /*
   ** Css resources
   */
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css'
  ],
  /*
   ** Style resources
   */
  styleResources: {
    scss: [
      'assets/partials/main.scss',
      'assets/partials/mixins.scss',
      'assets/partials/transitions.scss',
      'assets/partials/vars.scss',
      'assets/partials/animations.scss'
    ]
  },
  /*
   ** Axios module
   */
  axios: {
    progress: true,
    debug: true,
    browserBaseURL: 'http://51.15.109.100:8000/api/v1'
  },
  /*
   ** Build configuration
   */
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        '_': 'lodash'
      })
    ],
    extend(config, { isDev, isClient }) {

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }

    }
  }
}